//
//  News.swift
//  Gamesundbusiness
//
//  Created by Nizar Hamouda on 01/11/2018.
//  Copyright © 2018 Nizar Hamouda. All rights reserved.
//

import UIKit

class News: Codable {
    
    var newsId: Int
    var newsTitle: String
    var newsSubTitle: String
    var newsOriImage: String
    var newsThumbImage: String
    var newsShortDesc: String
    var newsBody: String
    var newsCat: String
    var newsUrl: String
    
    /*init(jsonDic : NSDictionary) {
        self.newsId = jsonDic["newsId"] as! Int
        self.newsTitle = jsonDic["newsTitle"] as! String
        self.newsSubTitle = jsonDic["newsSubTitle"] as! String
        self.newsOriImage = jsonDic["newsOriImage"] as! String
        self.newsThumbImage = jsonDic["newsThumbImage"] as! String
        self.newsShortDesc = jsonDic["newsShortDesc"] as! String
        self.newsBody = jsonDic["newsBody"] as! String
        self.newsCat = jsonDic["newsCat"] as! String
        self.newsUrl = jsonDic["newsUrl"] as! String
    }*/
    
    
     init(newsId: Int,
     newsTitle: String,
     newsSubTitle: String,
     newsOriImage: String,
     newsThumbImage: String,
     newsShortDesc: String,
     newsBody: String,
     newsCat: String,
     newsUrl: String ){
        self.newsId = newsId
        self.newsTitle = newsTitle
        self.newsSubTitle = newsSubTitle
        self.newsOriImage = newsOriImage
        self.newsThumbImage = newsThumbImage
        self.newsShortDesc = newsShortDesc
        self.newsBody = newsBody
        self.newsCat = newsCat
        self.newsUrl = newsUrl
    }
    
   
    
}
