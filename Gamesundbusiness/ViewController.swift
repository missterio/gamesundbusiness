//
//  ViewController.swift
//  Gamesundbusiness
//
//  Created by Nizar Hamouda on 01/11/2018.
//  Copyright © 2018 Nizar Hamouda. All rights reserved.
//

import UIKit
// import Alamofire
//import SwiftyJSON

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate,
UISearchBarDelegate{
 
    

    final let url =  URL(string: "https://www.gamesundbusiness.de/index.php?id=581")
    
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    var NewsArrayResult: [News] = []
    var news:[News] = []
    var currentNewsArray:  [News] = [] //updated table
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.hideKeyboardWhenTappedAround()
        downloadJson()
        tableView.dataSource = self
        tableView.separatorStyle = .none
        spinner.isHidden = false
        self.searchBar.delegate = self
        /*loadData(completition: { [weak self] in
            // completion
            self?.tableView.reloadData()
        })*/
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        let doneButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.done,
             target: self, action: #selector(self.doneClicked))
        toolBar.setItems([doneButton], animated: true)
        searchBar.inputAccessoryView = toolBar
            }
    @objc func doneClicked() {
        view.endEditing(true)
    }
    
    func downloadJson() {
        
        guard let downloadURL = url else {return}
        URLSession.shared.dataTask(with: downloadURL) { Data, URLResponse, Error in
            guard let data = Data, Error == nil, URLResponse != nil else {
                print("something is wrong")
                return }
            print("downloaded")
            do {
                let decoder = JSONDecoder()
                let downloadedNews = try decoder.decode([News].self , from: data)
                self.news = downloadedNews
                self.currentNewsArray = self.news
                DispatchQueue.main.async {
                    self.spinner.isHidden = true
                        self.tableView.reloadData()
                    self.emptyMessage()
                }
            
            } catch {
                print("something wrong after downloaded")
            }
        }.resume()
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return currentNewsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! NewsTableViewCell
        
        
        cell.setup(news: currentNewsArray[indexPath.row])
        
        return cell;
    }
    

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 125
    }
 
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.alpha = 0
        UIView.animate(withDuration: 1.0) {
            cell.alpha = 1.0
        }
        cell.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        UIView.animate(withDuration: 0.4) {
            cell.transform = CGAffineTransform.identity
        }
    }
    
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        guard !searchText.isEmpty else {
            currentNewsArray = news
            tableView.reloadData()
            emptyMessage()
            return
        }
        currentNewsArray = news.filter({ (News) -> Bool in
            return News.newsTitle.contains(searchText)
        })
        
        tableView.reloadData()
        emptyMessage()
    }
    
    func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        
    }
    
 

    
    // Navigation
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "showDetail"){
            let newsDetail = segue.destination as! NewsDetailViewController
            
            if let indexPath = self.tableView.indexPathForSelectedRow {
                let selectedNews = news[indexPath.row]
                newsDetail.news = selectedNews
            }
        }
    }
    
    private func emptyMessage() {
    
    if currentNewsArray.count == 0 {
    self.tableView.setEmptyMessage("No results")
    } else {
    self.tableView.restore()
    }
    
    }
    
    
    /*func loadData(completition: @escaping () -> ()){
        Alamofire.request(url, method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil).responseJSON {
            response in
            switch response.result {
            case .success:
                // print(response.result.value as Any)
                self.NewsArrayResult.removeAll()
                DispatchQueue.global(qos: .userInteractive).async {
                if let ReqResult = response.result.value {
                    let ParsedJSON = JSON(ReqResult)
                    
                    if let items = ParsedJSON.array {
                        for item in items {
                            
                            let p: NSDictionary = [
                                
                                "newsId": item["newsId"].int!,
                                "newsTitle": item["newsTitle"].string!,
                                "newsSubTitle": item["newsSubTitle"].string!,
                                "newsOriImage": item["newsOriImage"].string!,
                                "newsThumbImage": item["newsThumbImage"].string!,
                                "newsShortDesc": item["newsShortDesc"].string!,
                                "newsBody": item["newsBody"].string!,
                                "newsCat": item["newsCat"].string!,
                                "newsUrl": item["newsUrl"].string!,
                                ]
                            
                            
                            self.NewsArrayResult.append(News(jsonDic: p ))
                            
                            
                            
                        }
                    }
                    }
                    DispatchQueue.main.async {
                        //print(self.NewsArrayResult[0].newsBody)
                        //self.tableView.reloadData()
                        completition()
                    }
                }
                
            case .failure:
                print("Error !!")
            }
        }

    }
    */
    
   
    
    }




