//
//  NewsTableViewCell.swift
//  Gamesundbusiness
//
//  Created by Nizar Hamouda on 02/11/2018.
//  Copyright © 2018 Nizar Hamouda. All rights reserved.
//

import UIKit

class NewsTableViewCell: UITableViewCell {
    @IBOutlet weak var ThumbImage: UIImageView!
    @IBOutlet weak var Title: UILabel!
    @IBOutlet weak var ShortDescription: UILabel!
    @IBOutlet weak var Subtitle: UILabel!
    @IBOutlet weak var backgroundCardView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
      self.backgroundCardView.backgroundColor = UIColor.white
        contentView.backgroundColor = UIColor(red: 240/255.0, green: 240/255.0, blue: 240/255.0, alpha: 1.0)
        backgroundCardView.layer.cornerRadius = 3.0
        backgroundCardView.layer.masksToBounds = false
        backgroundCardView.layer.shadowColor = UIColor.black.withAlphaComponent(0.2).cgColor
        backgroundCardView.layer.shadowOffset = CGSize(width: 0, height: 0)
        backgroundCardView.layer.shadowOpacity = 0.8
        
    }

    func setup(news: News) {
      Title.text = news.newsTitle
      ShortDescription.text = news.newsShortDesc
      Subtitle.text = news.newsSubTitle
        /*let ThumbImageUrl = URL(string: "https://www.gamesundbusiness.de"+news.newsThumbImage)
        do{
            ThumbImage.image = UIImage(data: try NSData(contentsOf: ThumbImageUrl!) as Data)
        }
        catch{
            print("Error info: \(error)")
        }*/
        
        if let ThumbImageUrl = URL(string: "https://www.gamesundbusiness.de"+news.newsThumbImage) {
            DispatchQueue.global().async {
                let data = try? Data(contentsOf: ThumbImageUrl)
                if let data = data {
                    
                    let image = UIImage(data: data)
                    DispatchQueue.main.async {
                        self.ThumbImage.image = image
                    }
                }
            }
        }
    }

}
