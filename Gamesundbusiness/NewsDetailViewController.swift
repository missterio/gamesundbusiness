//
//  NewsDetailViewController.swift
//  Gamesundbusiness
//
//  Created by Nizar Hamouda on 02/11/2018.
//  Copyright © 2018 Nizar Hamouda. All rights reserved.
//

import UIKit

class NewsDetailViewController: UIViewController {
    
    
 
    @IBOutlet weak var newsTitle: UILabel!
    @IBOutlet weak var newsSubtitle: UILabel!
    @IBOutlet weak var newsImage: UIImageView!
    @IBOutlet weak var newsDescription: UILabel!
    

    
    var news:News?
    override func viewDidLoad() {
        super.viewDidLoad()
        newsTitle.text = news?.newsTitle
        newsSubtitle.text = news?.newsSubTitle
        newsDescription.text = news?.newsShortDesc
         let descriptionHeight = newsDescription.optimalHeight
         newsDescription.frame = CGRect(x: newsDescription.frame.origin.x, y: newsDescription.frame.origin.y, width: newsDescription.frame.width, height: descriptionHeight)
        
        //let titleHeight = newsTitle.optimalHeight
        //newsTitle.frame = CGRect(x: newsTitle.frame.origin.x, y: newsTitle.frame.origin.y, width: newsTitle.frame.width, height: titleHeight)
       /* let newsOriImageUrl = URL(string:
            "https://www.gamesundbusiness.de"+(news?.newsOriImage)!)
        do{
            newsImage.image = UIImage(data: try NSData(contentsOf: newsOriImageUrl!) as Data)
        }
        catch{
            print("Error info: \(error)")
        }*/
        
        if let newsOriImageUrl = URL(string: "https://www.gamesundbusiness.de"+(news?.newsOriImage)!) {
            DispatchQueue.global().async {
                let data = try? Data(contentsOf: newsOriImageUrl)
                if let data = data {
                    
                    let image = UIImage(data: data)
                    DispatchQueue.main.async {
                        self.newsImage.image = image
                    }
                }
            }
        }
        // Do any additional setup after loading the view.
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    

}
